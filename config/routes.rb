Rails.application.routes.draw do
  root 'home#app'

  get 'status', to: 'home#status'

  namespace :api, defaults: { format: :json } do
    resources :users, only: [:index, :show] do
      get 'me', on: :collection
      resources :frothies, only: [:index]
    end

    resources :frothies, only: [:show, :create]
  end

  devise_for :users, path: '', :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  devise_scope :user do
    get 'sign_in',
        :to => redirect('/auth/google_oauth2', status: 302),
        :as => :new_user_session
    get 'sign_out',
        :to => 'devise/sessions#destroy',
        :as => :destroy_user_session
  end

  get '*page', to: 'home#app', constraints: ->(request) do
    !request.xhr? && request.format.html?
  end
end
