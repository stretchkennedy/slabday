const { environment } = require('@rails/webpacker');

const css = environment.loaders.get('css');
const cssLoader = css.use.find(loader => loader.loader === 'css-loader');
Object.assign(
  cssLoader.options,
  {
    modules: true,
    sourceMap: true,
    importLoaders: 1,
    localIdentName: '[name]__[local]___[hash:base64:5]',
  },
);

module.exports = environment;
