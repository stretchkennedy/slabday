class UriAdapterWithoutContentDisposition < Paperclip::UriAdapter
  def filename_from_content_disposition
    nil
  end
end

UriAdapterWithoutContentDisposition.register
Paperclip::DataUriAdapter.register
