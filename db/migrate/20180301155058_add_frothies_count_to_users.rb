class AddFrothiesCountToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :frothies_count, :integer
    add_index :users, :frothies_count
  end
end
