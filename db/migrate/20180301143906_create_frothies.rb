class CreateFrothies < ActiveRecord::Migration[5.1]
  def change
    create_table :frothies do |t|
      t.attachment :proof
      t.references :user, foreign_key: true, null: false
      t.timestamps null: false
    end

    add_index :frothies, :created_at
  end
end
