class DefaultFrothiesCountToZero < ActiveRecord::Migration[5.1]
  def change
    change_column_default :users, :frothies_count, from: nil, to: 0
    change_column_null :users, :frothies_count, false, 0
  end
end
