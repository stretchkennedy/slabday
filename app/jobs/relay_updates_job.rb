class RelayUpdatesJob < ApplicationJob
  def perform(object)
    ActionCable.server.broadcast "all_updates",
                                 id: object.id,
                                 type: object.class.to_s
  end
end
