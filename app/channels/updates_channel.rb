class UpdatesChannel < ApplicationCable::Channel
  def subscribed
    stream_from "all_updates"
  end
end
