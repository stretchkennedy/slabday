class Frothy < ApplicationRecord
  belongs_to :user, counter_cache: true

  after_commit { RelayUpdatesJob.perform_later(self) }

  has_attached_file :proof,
                    styles: {
                      large: ['600x600>', :jpg],
                      medium: ['200x200>', :jpg],
                      small: ['100x100>', :jpg]
                    }
  validates_attachment_content_type :proof,
                                    content_type: /\A(image|video)\/.*\z/

  def proof_original
    proof.url
  end

  def proof_large
    proof.url(:large)
  end

  def proof_medium
    proof.url(:medium)
  end

  def proof_small
    proof.url(:small)
  end

  AS_JSON_DEFAULTS = {
    only: [:id, :created_at],
    methods: [:proof_original, :proof_large, :proof_medium, :proof_small]
  }

  def as_json(**args)
    super(AS_JSON_DEFAULTS.merge(args))
  end
end
