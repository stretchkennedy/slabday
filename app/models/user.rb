class User < ApplicationRecord
  has_many :frothies, ->{ order(created_at: :desc) }

  after_commit { RelayUpdatesJob.perform_later(self) }

  devise :omniauthable, :rememberable, :trackable,
         omniauth_providers: %w|google_oauth2|

  has_attached_file :avatar,
                    styles: {
                      large: '600x600>',
                      medium: '200x200>',
                      small: '100x100>'
                    }
  validates_attachment_content_type :avatar,
                                    content_type: /\Aimage\/.*\z/

  def avatar_original
    avatar.url
  end

  def avatar_large
    avatar.url(:large)
  end

  def avatar_medium
    avatar.url(:medium)
  end

  def avatar_small
    avatar.url(:small)
  end

  AS_JSON_DEFAULTS = {
    only: [:id, :name, :frothies_count],
    methods: [:avatar_original, :avatar_large, :avatar_medium, :avatar_small]
  }

  def as_json(**args)
    super(AS_JSON_DEFAULTS.merge(args))
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.name = auth.info.name
      user.avatar = URI.parse(auth.info.image)
    end
  end
end
