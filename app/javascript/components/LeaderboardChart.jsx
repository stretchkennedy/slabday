import React from 'react';
import PropTypes from 'prop-types';
import { BarChart } from 'react-easy-chart';
import Fetch from '../components/Fetch';

const LeaderboardChart = ({ width, height }) => (
  <Fetch url="/api/users">
    {({ data }) => (
      <BarChart
        axes
        colorBars
        width={width}
        height={height}
        yDomainRange={[0, 24]}
        data={data.map(({ name, frothies_count }) => ({ x: name, y: frothies_count }))}
      />
    )}
  </Fetch>
);

export default LeaderboardChart;
