import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardMedia, CardTitle } from 'react-toolbox/lib/card';
import styles from './Frothy.scss';

const friendlyTime = (t) => t.toLocaleTimeString(undefined, {
  formatMatcher: 'basic',
  hour: '2-digit',
  minute: '2-digit',
});

const Frothy = ({ id, to, proof, createdAt }) => (
  <Card key={id} className={styles.card}>
    <a href={to}>
      <CardMedia
        image={proof}
        aspectRatio="wide"
      />
      <CardTitle
        title={friendlyTime(createdAt)}
      />
    </a>
  </Card>
);

Frothy.propTypes = {
  id: PropTypes.number.isRequired,
  proof: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  createdAt: PropTypes.instanceOf(Date).isRequired,
};

export default Frothy;
