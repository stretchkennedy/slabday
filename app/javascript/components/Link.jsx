import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import MdLink from 'react-toolbox/lib/link';

class Link extends Component {
  handleClick = (e) => {
    const { history, to } = this.props;
    e.preventDefault();
    history.push(to);
  }

  render() {
    const {
      match,
      location,
      history,
      staticContext,
      to,
      ...props,
    } = this.props;
    return (
      <MdLink
        to={to}
        {...props}
        onClick={this.handleClick}
      />
    );
  }
}

export default withRouter(Link);
