import React, { Component } from 'react';
import Original from 'react-toolbox/lib/app_bar';
import { IconMenu, MenuItem } from 'react-toolbox/lib/menu';
import { Drawer } from 'react-toolbox/lib/drawer';
import { Card, CardMedia, CardTitle } from 'react-toolbox/lib/card';
import FontIcon from 'react-toolbox/lib/font_icon';
import Navigation from 'react-toolbox/lib/navigation';
import Button from 'react-toolbox/lib/button';
import MdLink from 'react-toolbox/lib/link';
import Snackbar from 'react-toolbox/lib/snackbar';
import ProgressBar from 'react-toolbox/lib/progress_bar';
import find from 'lodash/find';
import axios from 'axios';
import Link from './Link';
import Fetch from './Fetch';
import drawerTheme from './Drawer.css';
import styles from './AppBar.scss';

const getCSRFToken = () => find(
  document.getElementsByTagName('meta'),
  (meta) => meta.name === 'csrf-token'
).content;

class AppBar extends Component {
  state = {
    drawerActive: false,
    snackbar: null,
    response: null,
  }

  handleToggleDrawer = () => {
    this.setState({ drawerActive: !this.state.drawerActive });
  }

  handleSelectPhoto = () => {
    this._fileInputRef.click();
  }

  handleRegistrationFailure = () => {
    this.setState({
      snackbar: (
        <Snackbar
          active
          action='Retry'
          label='Failed to register frothy'
          timeout={5000}
          onClick={this.handleRegisterFrothy}
          onTimeout={this.handleCloseSnackbars}
          type='warning'
        />
      ),
    });
  }

  handleRegistrationSuccess = () => {
    this.setState({
      snackbar: (
        <Snackbar
          active
          action='Dismiss'
          label='Successfully registered frothy'
          timeout={2000}
          onClick={this.handleCloseSnackbars}
          onTimeout={this.handleCloseSnackbars}
          type='accept'
        />
      ),
    });
  }

  handleCloseSnackbars = () => {
    this.setState({ snackbar: null });
  }

  handleFrothyAttached = (e) => {
    this.setState(
      { frothy: e.target.files[0] },
      this.handleRegisterFrothy,
    );
  }

  handleRegisterFrothy = (e) => {
    this.setState({
      snackbar: (
        <Snackbar active><ProgressBar type="linear" mode="indeterminate" /></Snackbar>
      ),
    });

    const data = new FormData();
    data.append('frothy[proof]', this.state.frothy);

    axios.post('/api/frothies', data, {
      headers: {
        'X-CSRF-Token': getCSRFToken(),
        'X-Requested-With': 'XMLHttpRequest',
      },
      withCredentials: true,
      onUploadProgress: (e) => {
        if (!e.lengthComputable) return;
        this.setState({
          snackbar: (
            <Snackbar active>
              <ProgressBar
                max={e.total}
                value={e.loaded}
                type="linear"
                mode="determinate"
              />
            </Snackbar>
          ),
        });
      },
    })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('failed to upload frothy');
        }
      })
      .then(this.handleRegistrationSuccess)
      .catch(this.handleRegistrationFailure);
  }

  handleFileInputRef = (ref) => {
    this._fileInputRef = ref;
  }

  render() {
    return (
      <Fetch
        url="/api/users/me"
        handleFetching={false}
      >
        {({ data: { id, name, avatar_large } = {} }) => (
          <div>
            <Drawer
              onOverlayClick={this.handleToggleDrawer}
              active={this.state.drawerActive}
              theme={drawerTheme}
            >
              <Card>
                <CardMedia
                  image={avatar_large}
                  aspectRatio="wide"
                />
                <CardTitle
                  title={name}
                />
                <Navigation type="vertical">
                  <Link
                    to={`/contestants/${id}`}
                    icon="person"
                    label="Profile"
                  />
                  <MdLink
                    href="/sign_out"
                    icon="close"
                    label="Sign out"
                  />
                </Navigation>
              </Card>
            </Drawer>
            <Original
              fixed
              leftIcon="menu"
              onLeftIconClick={this.handleToggleDrawer}
              rightIcon="camera_alt"
              onRightIconClick={this.handleSelectPhoto}
              {...this.props}
            />
            <input
              className={styles.fileInput}
              type="file"
              ref={this.handleFileInputRef}
              accept="image/*,video/*;capture=camera"
              capture="camera"
              onChange={this.handleFrothyAttached}
            />
            {this.state.snackbar}
          </div>
        )}
      </Fetch>
    );
  }
}

AppBar.propTypes = Original.propTypes;

export default AppBar;
