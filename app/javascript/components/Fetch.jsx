import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Fetch as Original } from 'react-request';
import ActionCable from 'actioncable';

const Loading = () => <div className="loading" />;

const Failure = ({ error }) => <div>{JSON.stringify(error)}</div>;

const listeners = [];
const cable = ActionCable.createConsumer();
cable.subscriptions.create('UpdatesChannel', {
  received: (data) => {
    listeners.forEach(l => l());
  },
});

class Fetch extends Component {
  static propTypes = {
    ...Original.propTypes,
    handleFetching: PropTypes.bool.isRequired,
    handleErrors: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    handleErrors: true,
    handleFetching: true,
  }

  componentDidMount() {
    listeners.push(this.handleUpdate);
  }

  componentWillUnmount() {
    const index = listeners.indexOf(this.handleUpdate);
    if (index >= 0) {
      listeners.splice(index, 1);
    }
  }

  handleUpdate = () => {
    this._doFetch();
  }

  render() {
    const {
      handleErrors,
      handleFetching,
      children,
      ...props,
    } = this.props;
    return (
      <Original
        credentials="same-origin"
        fetchPolicy="cache-and-network"
        {...props}
      >
        {(response) => {
          const { failed, data, error, doFetch } = response;
          this._doFetch = doFetch;
          if (handleFetching && !data) return <Loading />;
          if (handleErrors && failed) return <Failure error={error || data} />;
          if (data) return children(response);
        }}
      </Original>
    );
  }
}

export default Fetch;
