import React from 'react';
import PropTypes from 'prop-types';
import { List, ListItem } from 'react-toolbox/lib/list';
import { Link } from 'react-router-dom';
import Fetch from '../components/Fetch';
import AppBar from '../components/AppBar';

const formatCount = (n) => {
  if (n === 24) return '24 down, and finished';
  if (n > 24) return `${n} down, plus ${n - 24} extra`;
  return `${n} down, ${24 - n} to go`
};

const Leaderboard = ({ history }) => (
  <div>
    <AppBar title="Slab Day leaderboard" />
    <Fetch url="/api/users">
      {({ data }) => (
        <List>
          {data.map(({ id, avatar_small, name, frothies_count }) => (
            <Link key={id} to={`/contestants/${id}`}>
              <ListItem
                avatar={avatar_small}
                caption={name}
                legend={formatCount(frothies_count)}
                rightIcon="chevron_right"
              />
            </Link>
          ))}
        </List>
      )}
    </Fetch>
  </div>
);

export default Leaderboard;
