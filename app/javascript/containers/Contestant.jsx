import React from 'react';
import PropTypes from 'prop-types';
import { List, ListItem } from 'react-toolbox/lib/list';
import { Link } from 'react-router-dom';
import LazyLoad from 'react-lazyload';
import Fetch from '../components/Fetch';
import AppBar from '../components/AppBar';
import Frothy from '../components/Frothy';
import styles from './Contestant.scss';

const Leaderboard = ({ match, history }) => (
  <Fetch url={`/api/users/${match.params.id}`}>
    {({ data }) => (
      <div>
        <AppBar
          title={data.name}
          leftIcon="navigate_before"
          onLeftIconClick={() => history.push('/')}
        />
        <div className={styles.container}>
          {data.frothies.length === 0 && (
            <h2>No frothies yet</h2>
          )}
          {data.frothies.map(({ id, proof_original, proof_large, created_at }) => (
            <LazyLoad height={407.5} offset={1000}>
              <Frothy
                id={id}
                key={id}
                proof={proof_large}
                to={proof_original}
                createdAt={new Date(created_at)}
              />
            </LazyLoad>
          ))}
        </div>
      </div>
    )}
  </Fetch>
);

export default Leaderboard;
