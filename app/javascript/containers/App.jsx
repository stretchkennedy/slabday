import 'babel-polyfill';
import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Leaderboard from './Leaderboard';
import Contestant  from './Contestant';
import Chart  from './Chart';
import styles from './App.scss';

const App = () => (
  <div>
    <BrowserRouter>
      <Switch>
        <Route exact path="/chart" component={Chart} />
        <div className={styles.layout}>
          <Route exact path="/" component={Leaderboard} />
          <Route path="/contestants/:id" component={Contestant} />
        </div>
      </Switch>
    </BrowserRouter>
  </div>
);

export default App;
