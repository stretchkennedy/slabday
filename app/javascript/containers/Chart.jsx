import React from 'react';
import sizeMe from 'react-sizeme';
import LeaderboardChart from '../components/LeaderboardChart';

const Chart = ({ size: { width, height } }) => (
  <div style={{ width: '100vw', height: '100vh', position: 'absolute' }}>
    <LeaderboardChart
      width={width - 10}
      height={height - 10}
    />
  </div>
);

export default sizeMe({ monitorHeight: true })(Chart);
