class HomeController < ApplicationController
  before_action :authenticate_user!, except: :status

  def app
    render 'home/app', layout: false
  end

  def status
    render json: {
             timestamp: ApplicationRecord.connection.execute('SELECT current_timestamp').first["now"]
           },
           layout: false
  end
end
