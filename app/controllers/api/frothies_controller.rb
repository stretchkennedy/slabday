class Api::FrothiesController < ApplicationController
  before_action :authenticate_user!

  def index
    render json:
             User
             .find(params[:user_id])
             .frothies
             .order(created_at: :desc)
  end

  def show
    render json: Frothy.find(params[:id])
  end

  def create
    render json: Frothy.create(user_id: current_user.id, proof: params[:frothy][:proof])
  end
end
