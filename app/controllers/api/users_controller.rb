class Api::UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    render json: User.all.order(frothies_count: :desc)
  end

  def show
    render json: User.find(params[:id]),
           include: {
             frothies: Frothy::AS_JSON_DEFAULTS,
           }
  end

  def me
    render json: current_user
  end
end
